<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata(){
        return view('halaman.register');
    }

    public function send(Request $request){
        //dd($request->all());

        $namadepan = $request["fname"];
        $namabelakang = $request["lname"];

        return view('halaman.welcome',compact('namadepan','namabelakang'));
    }
}
