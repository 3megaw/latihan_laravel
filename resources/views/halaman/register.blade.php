<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname"><br>
        <p>Gender:</p>
        <input type="radio" id="ml" name="rbgender" value="Male">
        <label for="ml">Male</label><br>
        <input type="radio" id="fm" name="rbgender" value="Female">
        <label for="fm">Female</label><br>
        <input type="radio" id="ot" name="rbgender" value="Other">
        <label for="ot">Other</label><br><br>
        <label for="nationally">Nationally:</label>
        <select id="nationally" name="nationally">
            <option value="">Indonesian</option>
            <option value="">Indian</option>
            <option value="">Japanese</option>
            <option value="">Korean</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" id="l1" name="l1" value="Bahasa Indonesia">
        <label for="l1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="l2" name="l2" value="English">
        <label for="l2">English</label><br>
        <input type="checkbox" id="l3" name="l3" value="Other">
        <label for="l3">Other</label><br><br>
        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="4" cols="50"></textarea><br><br>

        <input type="submit" value="kirim">
      </form>
</body>
</html>
